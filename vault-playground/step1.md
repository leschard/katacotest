The purpose of the Vault Playground is to provide a safe place to learn and experiment Vault.

## Launch Vault Server
1. Run a vault server :<br />
`docker run \
  -d \
  --name vault \
  --cap-add=IPC_LOCK \
  -e 'VAULT_DEV_ROOT_TOKEN_ID=[[HOST_SUBDOMAIN]]-[[KATACODA_HOST]]' \
  -p 8080:8200 \
  vault`{{execute}}

2. Export Vault address and Vault version :<br />
`export VAULT_ADDR="http://localhost:8080"`{{execute}}<br />
`export VAULT_VERSION=$(docker exec vault vault --version | cut -d ' ' -f2 | cut -c 2- )`{{execute}}

3. Install vault CLI :<br />
`wget https://releases.hashicorp.com/vault/$VAULT_VERSION/vault_"$VAULT_VERSION"_linux_amd64.zip && \
  unzip vault_"$VAULT_VERSION"_linux_amd64.zip && \
  chmod +x vault && \
  mv vault /usr/local/bin`{{execute}}

4. Activate vault CLI autocompletion :<br />
`vault --autocomplete-install`{{execute}}
`source ~/.bashrc`{{execute}}

## Vault UI

- Access on https://[[HOST_SUBDOMAIN]]-8080-[[KATACODA_HOST]].environments.katacoda.com

- Default token:
    * **Token :** `[[HOST_SUBDOMAIN]]-[[KATACODA_HOST]]`{{copy}}

## Vault CLI

[Official Documentation](https://www.vaultproject.io/docs/commands)
